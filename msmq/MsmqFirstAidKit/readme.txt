---------------------------
MSMQ First Aid Kit Freeware
Version 1.0, 2005/04/06
Copyright Cogin 2005
---------------------------


INCLUDED PROGRAMS
-----------------
MQDelete - deletes message from top of queue
MQCopy - copies top message from one queue to another
MQSave - saves body of top message to file
MQSend - sends message with specified label and body taken from file


HOW TO USE
----------
All tools are command line programs. 
For usage instructions just start them without any parameters.


NEED SOMETHING BETTER?
----------------------

QueueExplorer - Explorer-like administration utility for MSMQ.

QueueExplorer offers much more than built-in management console: 
- copying, moving and deleting one or more messages
- saving and loading from disk
- stress testing
- viewing full message bodies
- accessing remote queues
and more, in friendly explorer-like interface. 

http://www.cogin.com


LICENSE
-------
These tools are free to use and copy, as long as this file is included.
This software is provided as-is without warranties of any kind.

