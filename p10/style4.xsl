<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>  <body>
<h2>K00000000 - Systems - Choice</h2>

<table border="1">
<tr bgcolor="red">
	<th>Manufacturer Name</th>
	<th>Price</th>
	<th>Main Memory</th>
	<th>Core CPUs</th>
</tr>
  <xsl:for-each select=" server_systems / system ">
  <tr>
		<td><xsl:value-of select=" manufacturer "/></td>
		<td><xsl:value-of select=" main_memory "/></td>
		<td><xsl:value-of select=" core_cpus "/></td>
  <xsl:choose>
  <xsl:when test="price = 5900">
  <td bgcolor="gold">
  <xsl:value-of select="price"/>
  </td>
  </xsl:when>
  <xsl:otherwise>
  <td><xsl:value-of select="price"/></td>
 </xsl:otherwise>
 </xsl:choose>
 </tr>
 </xsl:for-each>
  </table>   </body>   </html>
</xsl:template>
</xsl:stylesheet>
