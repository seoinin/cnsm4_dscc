<?xml version="1.0" encoding="ISO-8859-1"?>
 <xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
<body>

<div style="font-family:Arial,helvetica,sans-serif;font-size:24pt;color:blue; background-color:yellow">
<xsl:for-each select="server_systems/system">
	<p><H1><xsl:value-of select="manufacturer"/></H1></p>
	 <p><H3><xsl:value-of select="price"/></H3></p>
</xsl:for-each>
</div>

 </body>
 </html>
</xsl:template>
</xsl:stylesheet>
