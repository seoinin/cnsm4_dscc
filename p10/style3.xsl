<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<body>
<h2>K00000000 - Systems (Price Condition)</h2>

<table border="1">
<tr bgcolor="yellow">
	<th>Manufacturer Name</th>
	<th>Price</th>
	<th>Main Memory</th>
	<th>Core CPUs</th>
</tr>

<xsl:for-each select=" server_systems / system ">

	<xsl:if test="core_cpus &gt;4">
		<tr>
		<td><xsl:value-of select=" manufacturer "/></td>
		<td><xsl:value-of select=" price "/></td>
		<td><xsl:value-of select=" main_memory "/></td>
		<td><xsl:value-of select=" core_cpus "/></td>
		</tr>
	</xsl:if>

</xsl:for-each>

</table>

</body>
</html>
</xsl:template>
</xsl:stylesheet>
